"""FIXME Project description."""

import json
import logging
import os
import typing

import flask
import flask_babel
import flask_limiter
import sqlalchemy
import sqlalchemy.orm
import werkzeug.exceptions


__all__ = [
	"create_app",
	"get_config"
]
__version__ = "0.0.1"


logging.basicConfig(
	format="[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
	level=getattr(
		logging,
		os.environ.get(
			"LOGGING_LEVEL",
			"DEBUG"
		)
	)
)


def get_config(
	skip_context_check: bool = False,
	config_location_envvar: str = "CONFIG_LOCATION",
	fallback_name: str = "config.json"
) -> typing.Dict[str, typing.Any]:
	"""Gets the current JSON config for this service.

	If this function is run in a Flask context and ``skip_context_check`` is
	:data:`True`, :attr:`flask.current_app.config` is returned. Otherwise, an
	attempt is made to use the ``config_location_envvar`` environment variable
	for the config file. If it exists, the location it points to will be used.
	Otherwise, the current working directory's ``fallback_name`` is used.

	:param config_location_envvar: The environment variable to use in order to
		find the custom config file. ``CONFIG_LOCATION`` by default.
	:param fallback_name: The filename to use when the environment variable is
		not set. Defaults to ``config.json``.

	.. warning::
		It's assumed that if the environment variable is set, the location is
		valid and the file exists. If it is not set, ``config.json`` must exist.

	:returns: The config, in a Python dictionary.
	"""

	if not skip_context_check and flask.has_app_context():
		return flask.current_app.config

	with open(
		os.environ.get(
			config_location_envvar,
			os.path.join(os.getcwd(), fallback_name)
		),
		"r",
		encoding="utf-8"
	) as config_file:
		return json.load(config_file)


def create_app() -> flask.Flask:
	"""Creates a :class:`Flask <flask.Flask>` app with basic settings already
	applied.

	Loads the config from the file located where the ``CONFIG_LOCATION``
	environment variable describes, or ``$current_working_directory/config.json``
	if it's not set.

	Registers all blueprints in :mod:`.views`.
	"""

	app = flask.Flask(__name__)

	with app.app_context():
		app.logger.info("Setting up app")

		app.logger.debug("Loading config file")

		app.config.update(get_config(skip_context_check=True))

		for environment_variable in (
			"BABEL_DEFAULT_LOCALE",

			"SECRET_KEY"
		):
			app.config[environment_variable] = os.environ[environment_variable]

		app.logger.debug("Creating engine")

		database_url = app.config["DATABASE_URL"]

		if database_url.startswith("postgres://"):
			database_url = database_url.replace("postgres://", "postgresql://")

		sa_engine = sqlalchemy.create_engine(database_url)

		app.sa_session_class = sqlalchemy.orm.scoped_session(
			sqlalchemy.orm.sessionmaker(
				bind=sa_engine
			)
		)

		app.logger.debug("Setting up extensions")

		babel = flask_babel.Babel(app)
		app.babel = babel

		app.limiter = flask_limiter.Limiter(
			app,
			key_func=flask.request.remote_addr,
			default_limits=["300 per day", "100 per hour"]
		)

		@babel.localeselector
		def get_locale() -> str:
			"""Gets the user's locale based on the current
			:attr:`request <flask.request>`. By default, ``cs`` is assumed, since
			that's the default language of our program.

			:returns: The locale.

			..seealso::
				Documentation: `https://python-babel.github.io/flask-babel/`_
			"""

			locale = flask.request.accept_languages.best_match(["cs"])

			return (
				locale
				if locale is not None
				else "cs"
			)

		@app.before_request
		def before_request() -> None:
			"""Creates a SQLAlchemy session at `g.sa_session <flask.g.sa_session>`
			from the :attr:`current_app <flask.current_app>`'s assigned
			:attr:`sa_session_class <flask.current_app.sa_session_class>` and sets
			:attr:`g.locale <flask.g.locale>` to that of the current user's. Or at
			least as close as we could get to it.
			"""

			flask.g.sa_session = flask.current_app.sa_session_class()
			flask.g.locale = get_locale()

		@app.teardown_request
		def teardown_request(
			exception: typing.Union[None, Exception]
		) -> None:
			"""Attempts to commit :attr:`flask.g.sa_session` and rolls it back if
			any exception is raised during the process. The exception is then
			logged.

			:param exception: The exception that occurred in the prior request,
				if there was any.
			"""

			if "sa_session" in flask.g:
				# "Clean" the session
				try:
					flask.g.sa_session.commit()
				except Exception as commit_exception:
					flask.g.sa_session.rollback()

					flask.current_app.logger.error(
						"Exception %s raised during the request teardown session commit: %s",
						commit_exception.__class__.__name__,
						(
							commit_exception
							if hasattr(commit_exception, "__str__")
							else "no details"
						)
					)

				flask.current_app.sa_session_class.remove()

		from .errorhandlers import handle_http_web_exception
		from .exceptions import WebException

		app.register_error_handler(
			werkzeug.exceptions.HTTPException,
			handle_http_web_exception
		)
		app.register_error_handler(
			WebException,
			handle_http_web_exception
		)

		# FIXME
		from .views import FIXME_blueprint

		for blueprint in (FIXME_blueprint,):
			app.logger.debug(
				"Registering blueprint: %s",
				blueprint
			)

			app.register_blueprint(blueprint)

		@app.cli.command("reflect")
		def reflect() -> None:
			"""Reflects database models."""

			from .database import Base

			Base.metadata.create_all(bind=sa_engine)

		return app
