"""Exception handlers."""

import typing

import flask
import werkzeug.exceptions

from .. import exceptions


__all__ = ["handle_http_web_exception"]
__version__ = "0.0.1"


def _log_exception(exception: Exception) -> None:
	"""Logs the ``exception``, if we are inside a Flask app context. The app's
	:attr:`logger <flask.current_app.logger>` is used.

	:param exception: The exception to log.
	"""

	if flask.has_app_context():
		flask.current_app.logger.info(
			"Handling exception %s: %s",
			exception.__class__.__name__,
			(
				exception
				if hasattr(exception, "__str__")
				else "no description"
			)
		)


def handle_http_web_exception(
	exception: typing.Union[
		werkzeug.exceptions.HTTPException,
		exceptions.WebException
	]
) -> typing.Tuple[flask.Response, int]:
	"""Turns an :class:`HTTPException <werkzeug.exceptions.HTTPException>`
	or :class:`WebException <..exceptions.WebException>` object into a rendered
	HTML template containing information about it.

	:param exception: The exception to convert.

	:returns: A tuple of the rendered ``exception.html`` template and the
		exception's status code.

	.. note::
		Flask's default error handler knows how to handle this type of exception
		already, but since it uses completely different HTML documents to do so,
		it would be inconsistent with the rest of the project.
	"""

	_log_exception(exception)

	return flask.render_template(
		"exception.html",
		name=exception.name,
		description=exception.description
	), exception.code
