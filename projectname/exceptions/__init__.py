"""Exceptions."""

from __future__ import annotations

import http.client

import flask_babel

__all__ = [
	"WebException",
	"WebRateLimitExceeded"
]
__version__ = "0.0.1"


class WebException(Exception):
	"""The base class for all web exceptions."""

	code = http.client.INTERNAL_SERVER_ERROR
	"""The HTTP error code of an exception. By default, this will be ``500``."""

	name = None
	"""The name of an exception. :data:`None` by default."""

	description = None
	"""The description of an exception. :data:`None` by default."""

	def __init__(
		self: WebException,
		name: str = name,
		description: str = description,
		**kwargs
	) -> None:
		"""Sets the :attr:`description <.WebException.description>` and
		:attr:`name <.WebException.name>` instance variables to the given
		values. If this method isn't used, they remain :data:`None`.

		:param name: The exception's name.
		:param description: The exception's description.
		"""

		self.name = name
		self.description = description

		super().__init__(**kwargs)

	def __str__(self: WebException) -> str:
		"""Turns this instance into a human-readable string.

		:returns: The string.
		"""

		description = (
			self.description
			if self.description is not None
			else "no description"
		)

		name = (
			self.name
			if self.name is not None
			else self.__class__.__name__
		)

		return f"{self.code} {name}: {description}"

	def __repr__(self: WebException) -> str:
		"""Turns this instance into a typical ``__repr__`` string.

		:returns: The string.
		"""

		return (
			f"<{self.__class__.__name__}("
			f"code={self.code}, "
			f"name={self.name}, "
			f"description={self.description}"
			")>"
		)


class WebRateLimitExceeded(WebException):
	"""Exception class for when a user has exceeded their rate limit for a
	specific view.

	.. seealso::
		:class:`FIXME.limiter.Limiter`
	"""

	name = flask_babel.lazy_gettext("Překročen limit požadavků")
	description = flask_babel.lazy_gettext(
		"Z tvé sítě jsme zaznamenali vysoké množství požadavků, které jsme "
		"vyhodnotili jako spam. Zkus se za chvíli připojit znovu, případně "
		"pro jistotu spusť antivirovou kontrolu."
	)

	code = http.client.TOO_MANY_REQUESTS
