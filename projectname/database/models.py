"""ORM database models."""

import sqlalchemy

from . import Base
from .utils import UUID, get_uuid

__all__ = ["Model"]


class Model(Base):
	"""FIXME Model type."""

	__tablename__ = "models"

	id = sqlalchemy.Column(
		UUID,
		primary_key=True,
		default=get_uuid
	)
