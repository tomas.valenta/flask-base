import typing
import uuid

import sqlalchemy

__all__ = [
	"UUID",
	"get_uuid"
]


class UUID(sqlalchemy.types.TypeDecorator):
	r"""A simple, portable UUID
	:class:`TypeDecorator <sqlalchemy.types.TypeDecorator>`.

	.. note::
		The original can be found in the `SQLAlchemy documentation <https://doc\
		s.sqlalchemy.org/en/14/core/custom_types.html#backend-agnostic-guid-typ\
		e>`_. Thanks to `zzzeek / Mike Bayer <https://github.com/sqlalchemy/sql\
		alchemy/commit/d01a6f088779bdf0bc118fa3ad09dc12dbd1baf5#diff-b204afd4bd\
		c7e703f5a49a64ae5945ba20a1f8179f3031923940fc702a02eb73>`_!
	"""

	cache_ok = True
	impl = sqlalchemy.types.TypeEngine
	python_type = uuid.UUID

	def load_dialect_impl(
		self,
		dialect: sqlalchemy.engine.Dialect
	):
		if dialect.name == "postgresql":
			return dialect.type_descriptor(sqlalchemy.dialects.postgresql.UUID())

		return dialect.type_descriptor(sqlalchemy.types.CHAR(32))

	def process_literal_param(
		self,
		value: typing.Union[None, str, uuid.UUID],
		dialect: sqlalchemy.engine.Dialect
	) -> typing.Union[None, str]:
		if value is not None:
			if isinstance(value, uuid.UUID):
				value = value.hex
			else:
				value = uuid.UUID(value).hex

			value = str(value)

		return value

	process_bind_param = process_literal_param

	def process_result_value(
		self,
		value: typing.Union[None, str, uuid.UUID],
		dialect: sqlalchemy.engine.Dialect
	) -> typing.Union[None, uuid.UUID]:
		if (
			value is not None and
			not isinstance(value, uuid.UUID)
		):
			value = uuid.UUID(value)

		return value


def get_uuid(ctx: sqlalchemy.engine.ExecutionContext) -> uuid.UUID:
	"""Keeps generating an UUID4, until it is not present in the context's
	current column.

	:param ctx: The current execution context.

	:returns: The generated UUID.
	"""

	while True:
		uuid4 = uuid.uuid4()

		if not ctx.connection.execute(
			sqlalchemy.select(ctx.current_column).
			where(ctx.current_column == uuid4).
			exists().
			select()
		).scalars().one():
			break

	return uuid4
