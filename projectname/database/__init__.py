"""SQLAlchemy database models and utilities for them."""

import sqlalchemy.orm

Base = sqlalchemy.orm.declarative_base()

from .utils import UUID, get_uuid
from .models import Model

__all__ = [
	"Model",
	"UUID",
	"get_uuid"
]
__version__ = "0.0.1"
